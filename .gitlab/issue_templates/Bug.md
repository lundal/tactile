### Description

<!-- Describe the bug. What is the observed behavior? What is the expected behavior? -->

<!-- Screenshots, screencasts or logs, if applicable -->

### System information

- Linux distribution: <!-- Fedora 40, Ubuntu 22.04, etc -->
- GNOME version: <!-- 45.3, 46.0, etc -->
- Windowing system: <!-- X11 / Wayland -->
